package com.gzzyy.unionchain.ui.base

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity

open class BaseActivity : AppCompatActivity() {

    companion object {
        val handler = Handler()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        supportActionBar?.hide()
        val window: Window = getWindow()
        //取消设置透明状态栏,使 ContentView 内容不再覆盖状态栏
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        //需要设置这个 flag 才能调用 setStatusBarColor 来设置状态栏颜色
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        //设置状态栏颜色
        window.setStatusBarColor(0xffffff)
        super.onCreate(savedInstanceState)
    }

    fun startActivity(clazz: Class<out BaseActivity>, vararg pairs: Pair<String, String>) {
        val intent = Intent(this@BaseActivity, clazz)
        for (pair in pairs) {
            intent.putExtra(pair.first, pair.second)
        }
        startActivity(intent)
    }
}