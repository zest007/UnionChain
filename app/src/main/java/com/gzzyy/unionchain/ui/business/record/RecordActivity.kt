package com.gzzyy.unionchain.ui.business.record

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.gzzyy.unionchain.App
import com.gzzyy.unionchain.data.ObjectBox
import com.gzzyy.unionchain.data.bean.TransactionRecord_
import com.gzzyy.unionchain.databinding.ActivityRecordBinding
import com.gzzyy.unionchain.ui.base.BaseActivity
import com.gzzyy.unionchain.ui.business.record.adapter.RecordAdapter
import com.gzzyy.unionchain.ui.business.record.adapter.RecordBean
import kotlinx.coroutines.*

class RecordActivity : BaseActivity() {
    private val walletAddress = App.user?.walletAddress ?: ""
    private lateinit var recordAdapter: RecordAdapter
    private lateinit var binding: ActivityRecordBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRecordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toolbar.setNavigationOnClickListener {
            finish()
        }
        binding.tabMenu.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                createJob(tab?.position)
            }
        })
        binding.searchBlockBtn.setOnClickListener {
            startActivity(BlockListActivity::class.java)
        }
        binding.recordRv.layoutManager = LinearLayoutManager(baseContext)
        recordAdapter =
            RecordAdapter()
        binding.recordRv.adapter = recordAdapter
        createJob(0)
    }

    private var job1: Job? = null
    private var job2: Job? = null
    private var job3: Job? = null
    private fun createJob(type: Int?) {
        cancelAllJob()
        val recordBox = ObjectBox.getRecordBox()
        val query = recordBox.query()
        when (type) {
            0 -> {
                job1 = GlobalScope.launch {
                    val queryBuilder =
                        query.equal(TransactionRecord_.from, walletAddress).or().equal(
                            TransactionRecord_.to, walletAddress
                        ).orderDesc(TransactionRecord_.height).build()
                    val recordList = queryBuilder.find()
                    val list: MutableList<RecordBean> =
                        MutableList<RecordBean>(recordList.size) {
                            RecordBean.turnToRecordBean(recordList[it], walletAddress)
                        }
                    withContext(Dispatchers.Main) {
                        recordAdapter.setNewData(list)
                    }
                }
            }
            1 -> {
                job2 = GlobalScope.launch {
                    val queryBuilder =
                        query.equal(TransactionRecord_.from, walletAddress)
                            .orderDesc(TransactionRecord_.height).build()
                    val recordList = queryBuilder.find()
                    val list: MutableList<RecordBean> =
                        MutableList<RecordBean>(recordList.size) {
                            RecordBean.turnToRecordBean(recordList[it], walletAddress)
                        }
                    withContext(Dispatchers.Main) {
                        recordAdapter.setNewData(list)
                    }
                }
            }
            2 -> {
                job3 = GlobalScope.launch {
                    val queryBuilder =
                        query.equal(TransactionRecord_.to, walletAddress)
                            .orderDesc(TransactionRecord_.height).build()
                    val recordList = queryBuilder.find()
                    val list: MutableList<RecordBean> =
                        MutableList<RecordBean>(recordList.size) {
                            RecordBean.turnToRecordBean(recordList[it], walletAddress)
                        }
                    withContext(Dispatchers.Main) {
                        recordAdapter.setNewData(list)
                    }
                }
            }
        }
    }

    private fun cancelAllJob() {
        job1?.cancel()
        job2?.cancel()
        job3?.cancel()
    }

    override fun onDestroy() {
        super.onDestroy()
        cancelAllJob()
    }
}