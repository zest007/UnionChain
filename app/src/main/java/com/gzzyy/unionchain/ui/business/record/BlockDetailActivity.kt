package com.gzzyy.unionchain.ui.business.record

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.alibaba.fastjson.JSON
import com.gzzyy.unionchain.data.bean.TransferBeanJ
import com.gzzyy.unionchain.databinding.ActivityBlockDetailBinding
import com.gzzyy.unionchain.ui.base.BaseActivity
import com.gzzyy.unionchain.ui.business.record.adapter.TransactionInBlockAdapter

class BlockDetailActivity : BaseActivity() {
    companion object {
        const val BlockJson = "block_json"
    }

    private lateinit var binding: ActivityBlockDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBlockDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toolbar.setNavigationOnClickListener { finish() }
        val str = intent.getStringExtra(BlockJson)
        if (str != null) {
            val jsonObj = JSON.parseObject(str)
            binding.hashTv.text = jsonObj.getString("hash")
            binding.prevhashTv.text = jsonObj.getString("prevhash")
            binding.merklerootTv.text = jsonObj.getString("merkleroot")
            binding.nonceTv.text = jsonObj.getString("nonce")
            binding.heightTv.text = jsonObj.getIntValue("height").toString()
            binding.createTimeTv.text =
                RecordConstants.getDateStr(jsonObj.getLongValue("create_time"))
            val transactions = jsonObj.getString("transactions")
            if (transactions.startsWith("[")) {
                val transferList = JSON.parseArray(transactions, TransferBeanJ::class.java)
                if (transferList.isNotEmpty()) {
                    binding.transactionRv.layoutManager = LinearLayoutManager(baseContext)
                    val adapter =
                        TransactionInBlockAdapter()
                    binding.transactionRv.adapter = adapter
                    adapter.setNewData(transferList)
                }
            } else {
                binding.firstBlockTv.visibility = View.VISIBLE
            }
        }
    }
}