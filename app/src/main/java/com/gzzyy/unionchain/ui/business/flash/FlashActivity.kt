package com.gzzyy.unionchain.ui.business.flash

import android.os.Bundle
import android.os.Handler
import com.gzzyy.unionchain.App
import com.gzzyy.unionchain.databinding.ActivityFlashBinding
import com.gzzyy.unionchain.ui.base.BaseActivity
import com.gzzyy.unionchain.ui.business.login.LoginChooseActivity
import com.gzzyy.unionchain.ui.business.main.MainActivity

class FlashActivity : BaseActivity() {

    companion object {
        val handler = Handler()
    }

    private lateinit var binding: ActivityFlashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        handler.postDelayed({
            if (App.user != null) {
                startActivity(MainActivity::class.java)
            } else {
                startActivity(LoginChooseActivity::class.java)
            }
            finish()
        }, 1500)
    }
}