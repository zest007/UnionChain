package com.gzzyy.unionchain.ui.business.record.adapter

import androidx.annotation.LayoutRes
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gzzyy.unionchain.R
import com.gzzyy.unionchain.data.bean.TransferBeanJ
import com.gzzyy.unionchain.ui.business.record.RecordConstants

class TransactionInBlockAdapter(@LayoutRes layoutId: Int = R.layout.item_transaction_in_block) :
    BaseQuickAdapter<TransferBeanJ, BaseViewHolder>(layoutId) {
    override fun convert(helper: BaseViewHolder, item: TransferBeanJ) {
        helper.setText(R.id.hashTv, item.hash)
            .setText(R.id.prevhashTv, item.prevhash)
            .setText(R.id.fromTv, item.from)
            .setText(R.id.toTv, item.to)
            .setText(R.id.moneyTv, item.money.toString())
            .setText(R.id.nonceTv, item.nonce)
            .setText(R.id.heightTv, item.height.toString())
            .setText(R.id.createTimeTv, RecordConstants.getDateStr(item.create_time))
    }
}