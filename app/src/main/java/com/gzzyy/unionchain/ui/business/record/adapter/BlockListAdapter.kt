package com.gzzyy.unionchain.ui.business.record.adapter

import androidx.annotation.LayoutRes
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gzzyy.unionchain.R
import com.gzzyy.unionchain.data.bean.Block
import com.gzzyy.unionchain.ui.business.record.RecordConstants
import java.text.SimpleDateFormat
import java.util.*

class BlockListAdapter(@LayoutRes layoutId: Int = R.layout.item_block_list) :
    BaseQuickAdapter<Block, BaseViewHolder>(layoutId) {

    override fun convert(helper: BaseViewHolder, item: Block) {
        helper.setText(R.id.hashTv, "哈希值: ${item.hash}")
            .setText(R.id.merklerootTv, "墨客树: ${item.merkleroot}")
            .setText(R.id.createTimeTv, RecordConstants.getDateStr(item.create_time))
    }
}