package com.gzzyy.unionchain.ui.business.record

import java.text.SimpleDateFormat
import java.util.*

object RecordConstants {
    private val dateFormat: SimpleDateFormat =
        SimpleDateFormat("yyyy年MM月dd日HH时mm分ss秒", Locale.CHINA)

    fun getDateStr(createTime: Long): String {
        return dateFormat.format(Date(createTime * 1000))
    }
}