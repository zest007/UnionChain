package com.gzzyy.unionchain.ui.business.main

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import com.blankj.utilcode.util.ToastUtils
import com.gzzyy.unionchain.App
import com.gzzyy.unionchain.LiveEvent
import com.gzzyy.unionchain.data.ObjectBox
import com.gzzyy.unionchain.data.bean.TransactionRecord
import com.gzzyy.unionchain.databinding.ActivityMainBinding
import com.gzzyy.unionchain.ui.base.BaseActivity
import com.gzzyy.unionchain.ui.business.record.RecordActivity
import com.gzzyy.unionchain.ui.business.transfer.TransferActivity
import com.gzzyy.unionchain.util.KeyUtil
import com.jeremyliao.liveeventbus.LiveEventBus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : BaseActivity() {

    private var dataSyncEnd = false

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Log.e("test", "onCreate")
        GlobalScope.launch {
            val transferBeanBox = ObjectBox.getRecordBox()
            val list = transferBeanBox.query().build().find()
            Log.e("test", "size = " + list.size)
            for (transfer in list) {
                Log.e("test", "$transfer")
            }
        }
        binding.walletAddressTv.text = App.user?.walletAddress

        binding.walletTransportBtn.setOnClickListener {
            if (dataSyncEnd) {
                startActivity(TransferActivity::class.java)
            } else {
                ToastUtils.showShort("数据同步中，暂不能发起转账")
            }
        }
        binding.walletQrcodeBtn.setOnClickListener {
            if (dataSyncEnd) {
                startActivity(RecordActivity::class.java)
            } else {
                ToastUtils.showShort("数据同步中，暂不能收款")
            }
        }

        binding.copyImageView.setOnClickListener {
            //获取剪贴板管理器：
            val cm: ClipboardManager =
                getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            // 创建普通字符型ClipData
            val mClipData = ClipData.newPlainText("Label", binding.walletAddressTv.text)
            // 将ClipData内容放到系统剪贴板里。
            cm.setPrimaryClip(mClipData)
            ToastUtils.showShort("地址已复制")
        }

        LiveEventBus.get(LiveEvent.GetTransferListEnd).observeStickyForever {
            binding.dataSyncingTv.visibility = View.GONE
            dataSyncEnd = true
            updateBalance()
            Log.e("test", "GetTransferListEnd " + App.user?.walletAddress)
        }

        LiveEventBus.get(LiveEvent.SaveSingleEnd).observeForever {
            updateBalance()
        }
    }

    private fun updateBalance() {
        GlobalScope.launch {
            val balance = KeyUtil.findBalance(App.user?.walletAddress)
            Log.e("test", "balance = $balance")
            withContext(Dispatchers.Main) {
                if (balance > 0) {
                    binding.balanceTv.text = balance.toString()
                } else {
                    binding.balanceTv.text = ""
                }
            }
        }
    }
}
