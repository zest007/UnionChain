package com.gzzyy.unionchain.ui.business.login

import android.os.Bundle
import com.gzzyy.unionchain.ui.base.BaseActivity
import com.gzzyy.unionchain.databinding.ActivityLoginChooseBinding


class LoginChooseActivity : BaseActivity() {

    private lateinit var binding: ActivityLoginChooseBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginChooseBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.createWalletBtn.setOnClickListener {
            startActivity(CreateWalletActivity::class.java)
            finish()
        }
        binding.importWalletBtn.setOnClickListener {

        }
    }
}