package com.gzzyy.unionchain.ui.business.record.adapter


import androidx.annotation.LayoutRes
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gzzyy.unionchain.R
import com.gzzyy.unionchain.data.bean.TransactionRecord

class RecordAdapter(@LayoutRes layoutId: Int = R.layout.item_transaction_record) :
    BaseQuickAdapter<RecordBean, BaseViewHolder>(layoutId) {
    override fun convert(helper: BaseViewHolder, item: RecordBean) {
        helper.setText(R.id.addressTv, item.otherAddress)
        if (item.isFrom) {
            helper.setText(R.id.toFromTv, "From")
            helper.setTextColor(R.id.transferValueTv, R.color.red_light)
            helper.setText(R.id.transferValueTv, "+${item.value}")
        } else {
            helper.setText(R.id.toFromTv, "To")
            helper.setTextColor(R.id.transferValueTv, R.color.green_light)
            helper.setText(R.id.transferValueTv, "-${item.value}")
        }
    }
}

data class RecordBean(var isFrom: Boolean, var otherAddress: String, var value: Float) {
    companion object {
        fun turnToRecordBean(transactionRecord: TransactionRecord, address: String?): RecordBean {
            val isFrom = transactionRecord.to == address
            val otherAddress = if (isFrom) {
                transactionRecord.from
            } else {
                transactionRecord.to
            }
            return RecordBean(isFrom, otherAddress, transactionRecord.money)
        }
    }
}