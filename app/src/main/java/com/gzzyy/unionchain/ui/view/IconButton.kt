package com.gzzyy.unionchain.ui.view

import android.content.Context
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import com.gzzyy.unionchain.R

class IconButton(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
    androidx.appcompat.widget.AppCompatButton(context, attrs, defStyleAttr) {
    constructor(context: Context) : this(context, null,0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs,
        R.attr.buttonStyle
    )

    enum class DrawablePosition { NONE, LEFT, RIGHT, LEFT_AND_RIGHT }

    private var drawableWidth: Int = 0
    private var iconPadding: Int = 0
    private lateinit var position: DrawablePosition
    private val bounds: Rect = Rect()

    init {
        applyAttributes(attrs)
    }

    private fun applyAttributes(attrs: AttributeSet?) {
        val typeArray = context.obtainStyledAttributes(attrs,
            R.styleable.IconButton
        )
        val padding = typeArray.getDimensionPixelSize(R.styleable.IconButton_iconPadding, 0)
        setIconPadding(padding)
        typeArray.recycle()
    }

    private fun setIconPadding(padding: Int) {
        iconPadding = padding
        requestLayout()
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        val textPaint: Paint = paint
        val text = text.toString()
        textPaint.getTextBounds(text, 0, text.length, bounds)
        val textWidth = bounds.width()
        val factor = if (position === DrawablePosition.LEFT_AND_RIGHT) 2 else 1
        val contentWidth = drawableWidth + iconPadding * factor + textWidth
        val horizontalPadding = (width / 2.0 - contentWidth / 2.0).toInt()
        compoundDrawablePadding = -horizontalPadding + iconPadding
        when (position) {
            DrawablePosition.LEFT -> setPadding(
                horizontalPadding,
                paddingTop,
                0,
                paddingBottom
            )
            DrawablePosition.RIGHT -> setPadding(
                0,
                paddingTop,
                horizontalPadding,
                paddingBottom
            )
            DrawablePosition.LEFT_AND_RIGHT -> setPadding(
                horizontalPadding,
                paddingTop,
                horizontalPadding,
                paddingBottom
            )
            else -> setPadding(0, paddingTop, 0, paddingBottom)
        }
    }


    override fun setCompoundDrawablesWithIntrinsicBounds(
        left: Drawable?, top: Drawable?, right: Drawable?, bottom: Drawable?
    ) {
        super.setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom)
        if (left != null && right != null) {
            drawableWidth = left.intrinsicWidth + right.intrinsicWidth
            position =
                DrawablePosition.LEFT_AND_RIGHT
        } else if (left != null) {
            drawableWidth = left.intrinsicWidth
            position = DrawablePosition.LEFT
        } else if (right != null) {
            drawableWidth = right.intrinsicWidth
            position =
                DrawablePosition.RIGHT
        } else {
            position = DrawablePosition.NONE
        }
        requestLayout()
    }

}