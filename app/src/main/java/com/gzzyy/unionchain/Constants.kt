package com.gzzyy.unionchain

object LiveEvent {

    const val ContactUpdate = "contact_update"

    const val ContactBatchUpdate = "contact_batch_update"

    const val UpdateMyUuid = "update_my_uuid"

    const val DataChannelSendMsg = "data_channel_send_message"
    const val DataChannelReceiveMsg = "data_channel_receive_message"

    const val CommitTransfer = "commit_transfer"

    const val MessageToServer = "message_to_server"

    const val MessageToOther = "message_to_other"

    const val TransferResponse = "transfer_response"

    const val RequestTransferList = "request_transfer_list"

    const val GetTransferListEnd = "get_transfer_list_end"

    const val SaveData = "save_data"

    const val SaveSingleEnd = "save_single_end"

}
