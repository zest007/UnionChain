package com.gzzyy.unionchain.data

import android.content.Context
import com.gzzyy.unionchain.data.bean.Block
import com.gzzyy.unionchain.data.bean.MyObjectBox
import com.gzzyy.unionchain.data.bean.TransactionRecord
import io.objectbox.Box
import io.objectbox.BoxStore

object ObjectBox {
    lateinit var boxStore: BoxStore
        private set

    fun init(context: Context) {
        boxStore = MyObjectBox.builder()
            .androidContext(context.applicationContext)
            .build()
    }

    fun getRecordBox(): Box<TransactionRecord> {
        return boxStore.boxFor(TransactionRecord::class.java)
    }

    fun getBlockBox(): Box<Block> {
        return boxStore.boxFor(Block::class.java)
    }
}