package com.gzzyy.unionchain.data.bean

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

@Entity
data class Block(
    var hash: String,
    var nonce: String,
    var prevhash: String,
    var height: Int,
    var merkleroot: String,
    var create_time: Long,
    var transactions: String,
    @Id var id: Long = 0
)