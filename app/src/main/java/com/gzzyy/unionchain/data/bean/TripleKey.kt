package com.gzzyy.unionchain.data.bean

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

@Entity
data class TripleKey(
    var walletAddress: String?,
    var priKey: String?,
    var pubKey: String?,
    @Id var id: Long = 0
)