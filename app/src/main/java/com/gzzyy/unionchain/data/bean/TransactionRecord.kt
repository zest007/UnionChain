package com.gzzyy.unionchain.data.bean

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

@Entity
data class TransactionRecord(
    var from: String = "",//支出方
    var to: String = "",//收款方
    var money: Float = 0f,//交易金额
    var hash: String = "",//交易hash
    var from_utxo: Float = 0f,//支出方本次交易后余额
    var to_utxo: Float = 0f,//收款方本次交易后余额
    var hash_sign: String = "",//本次hash签名
    var nonce: String = "",//随机字符串
    var prevhash: String = "",//前一个hash
    var height: Int = 0,//高度
    var create_time: Long = 0L,//创建时间
    @Id var id: Long = 0
)