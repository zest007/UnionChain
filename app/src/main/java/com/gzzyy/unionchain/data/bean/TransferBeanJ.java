package com.gzzyy.unionchain.data.bean;

import androidx.annotation.NonNull;

public class TransferBeanJ {
    public @NonNull
    String from = "";//支出方
    public @NonNull
    String to = ""; //收款方
    public float money;//交易金额
    public float from_utxo;//支出方本次交易后余额
    public float to_utxo;//收款方本次交易后余额
    public @NonNull
    String hash = "";//交易hash
    public @NonNull
    String hash_sign = "";//本次hash签名
    public @NonNull
    String nonce = "";//随机字符串
    public @NonNull
    String prevhash = "";//前一个hash
    public int height;//高度
    public long create_time;//创建时间

    public TransactionRecord turnToTransactionRecord() {
        return new TransactionRecord(from,
                to,
                money,
                hash,
                from_utxo,
                to_utxo,
                hash_sign,
                nonce,
                prevhash,
                height,
                create_time, 0);
    }
}
