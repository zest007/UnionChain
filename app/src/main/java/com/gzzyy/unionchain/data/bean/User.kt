package com.gzzyy.unionchain.data.bean

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

@Entity
data class User(var walletAddress: String, var safeUserKey: String, @Id var id: Long = 0)