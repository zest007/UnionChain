package com.gzzyy.unionchain.data

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope

class FetchBlockWorker(appContext: Context, workerParam: WorkerParameters) :
    CoroutineWorker(appContext, workerParam) {
    override val coroutineContext = Dispatchers.IO

    override suspend fun doWork(): Result = coroutineScope {

        Result.success()
    }
}