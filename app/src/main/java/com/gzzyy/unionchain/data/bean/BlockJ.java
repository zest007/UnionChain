package com.gzzyy.unionchain.data.bean;

import androidx.annotation.NonNull;

public class BlockJ {
    @NonNull
    public String hash = "";
    @NonNull
    public String nonce = "";
    @NonNull
    public String prevhash = "";
    public int height;
    @NonNull
    public String merkleroot = "";
    public long create_time;
    @NonNull
    public String transactions = "";

    public Block turnToBlock() {
        return new Block(hash, nonce, prevhash, height, merkleroot, create_time, transactions, 0);
    }
}
