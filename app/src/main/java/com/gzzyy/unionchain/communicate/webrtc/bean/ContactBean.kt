package com.gzzyy.unionchain.communicate.webrtc.bean

class ContactBean(var uuid: String, var isConnect: Boolean = true) {
    override fun equals(other: Any?): Boolean {
        if (other != null && other is ContactBean) {
            return this.uuid == other.uuid
        }
        return false
    }

}