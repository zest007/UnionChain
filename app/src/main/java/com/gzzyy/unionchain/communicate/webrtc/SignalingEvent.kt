package com.gzzyy.unionchain.communicate.webrtc

enum class SignalingEvent {
    ConnectChange, IdRequest, IdReport, ConnectTo, Message
}