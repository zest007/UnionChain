package com.gzzyy.unionchain.communicate

enum class Action(val value: String) {
    Transfer("transfer"),
    GetTransfer("gettransfer"),
    GetBlock("getblock"),
    SyncBlock("syncblock")
}