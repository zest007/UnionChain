package com.gzzyy.unionchain.communicate.bean

class PageInfo(private var page_total: Int, var counts: Int, var page: Int, var page_size: Int) {
    fun hasNextPage(): Boolean {
        return page < page_total
    }

    override fun toString(): String {
        return "$page_total : $counts : $page : $page_size "
    }
}