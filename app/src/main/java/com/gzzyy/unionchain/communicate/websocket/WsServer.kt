package com.gzzyy.unionchain.communicate.websocket

import android.util.Log
import org.java_websocket.WebSocket
import org.java_websocket.drafts.Draft
import org.java_websocket.handshake.ClientHandshake
import org.java_websocket.server.WebSocketServer
import java.net.InetSocketAddress

class WsServer(
    address: InetSocketAddress,
    decoderCount: Int,
    drafts: List<Draft>?,
    connectionscontainer: Collection<WebSocket>
) :
    WebSocketServer(address, decoderCount, drafts, connectionscontainer) {

    companion object {
        const val TAG = "WsServer"
    }

    constructor(address: InetSocketAddress, decoderCount: Int, drafts: List<Draft>?) : this(
        address,
        decoderCount,
        drafts,
        HashSet<WebSocket>()

    )

    constructor(address: InetSocketAddress, drafts: List<Draft>) : this(
        address,
        Runtime.getRuntime().availableProcessors(),
        drafts
    )

    constructor(
        address: InetSocketAddress,
        decoderCount: Int
    ) : this(address, decoderCount, null)

    constructor(address: InetSocketAddress) : this(
        address,
        Runtime.getRuntime().availableProcessors(),
        null
    )

    override fun onOpen(conn: WebSocket?, handshake: ClientHandshake?) {
        Log.i(TAG, "onOpen")
    }

    override fun onClose(conn: WebSocket?, code: Int, reason: String?, remote: Boolean) {
        Log.i(TAG, "onOpen")

    }

    override fun onMessage(conn: WebSocket?, message: String?) {
        Log.i(TAG, "onOpen")

    }

    override fun onStart() {
        Log.i(TAG, "onOpen")

    }

    override fun onError(conn: WebSocket?, ex: Exception?) {
        Log.i(TAG, "onError")

    }
}